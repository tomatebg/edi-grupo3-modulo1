#include "../RotaDeOnibus/RotaDeOnibus.h"

typedef struct nodoRota {
  Lista * rotaDescritor;
  char nomeRota[50];
  struct nodoRota * prox;
}
ItemListaDeRotas;

typedef struct nodoRota * ListaDeRotas;

ListaDeRotas * cria_lista_de_rotas();
void libera_lista_de_rotas(ListaDeRotas * li);
ItemListaDeRotas * aloca_nodo_lista_de_rotas();
int insere_lista_de_rotas(ListaDeRotas * li, struct nodoRota al);
int remove_lista_de_rotas_inicio(ListaDeRotas * li);
int imprime_lista_de_rotas(ListaDeRotas * li, int modo);
struct nodoRota gera_nodo_lista_de_rotas(Lista * mat, char nRota[50]);
int remove_indice_lista_de_rotas(ListaDeRotas * li, int indice);
