#include <stdio.h>

#include <string.h>

#include <stdlib.h>

#include "ListaDeRotas.h"


ListaDeRotas * cria_lista_de_rotas() {
  ListaDeRotas * li = (ListaDeRotas * ) malloc(sizeof(ListaDeRotas));
  if (li != NULL)
    *
    li = NULL;
  return li;
}

void libera_lista_de_rotas(ListaDeRotas * li) {
  if (li != NULL) {
    ItemListaDeRotas * no;
    while (( * li) != NULL) {
      no = * li;
      * li = ( * li) -> prox;
      free(no);
    }
    free(li);
  }
}

ItemListaDeRotas * aloca_nodo_lista_de_rotas() {
  ItemListaDeRotas * el = (ItemListaDeRotas * ) malloc(sizeof(ItemListaDeRotas));
  return el;
}

int insere_lista_de_rotas(ListaDeRotas * li, struct nodoRota al) {
  //sempre no final, por ordem de cria��o
  ItemListaDeRotas * aux;
  if (li == NULL)
    return 0;
  ItemListaDeRotas * no = aloca_nodo_lista_de_rotas();
  if (no == NULL)
    return 0;
  no -> rotaDescritor = al.rotaDescritor;
  strcpy(no -> nomeRota, al.nomeRota);
  no -> prox = NULL;

  if (( * li) == NULL) { //lista vazia: insere in?cio
    * li = no;
  } else {

    aux = * li;
    while (aux -> prox != NULL) {

      aux = aux -> prox;
    }

    aux -> prox = no;
  }
  return 1;
}

int remove_lista_de_rotas_inicio(ListaDeRotas * li) {
  if (li == NULL) // Verifica se lista existe
    return 0;
  if (( * li) == NULL) //Verifica se a lista ? vazia
    return 0;

  ItemListaDeRotas * no = * li; //Declara n?
  * li = no -> prox;
  free(no);
  return 1;
}

int remove_indice_lista_de_rotas(ListaDeRotas * li, int indice) {
  if (li == NULL) // Verifica se lista existe
    return 0;
  if (( * li) == NULL) //Verifica se a lista ? vazia
    return 0;
  ItemListaDeRotas * ant, * no = * li;
  int cont = 1;
  while (no != NULL && indice != cont) {
    ant = no;
    no = no -> prox;
    cont++;
  }
  if (no == NULL) //Chega ao final
    return 0;

  if (no == * li) {
    * li = no -> prox; //apenas um nodoMatriz
  } else {
    ant -> prox = no -> prox;
  }
  free(no);
  return 1;
}

int tamanho_lista_de_rotas(ListaDeRotas * li) {
  if (li == NULL)
    return 0;
  int cont = 0;
  ItemListaDeRotas * no = * li;
  while (no != NULL) {

    cont++;
    no = no -> prox;
  }
  return cont;
}

int imprime_lista_de_rotas(ListaDeRotas * li, int modo) {
  int aux;
  if (li == NULL)
    return 0;
  int i = 0;

  int tam = tamanho_lista_de_rotas(li);
  printf("A Lista de Rotas possui %d elementos \n", tam);

  ItemListaDeRotas * calc = * li;
  int count = 1;
  if (calc == NULL) {
    printf("\nLista de Rotas vazia\n");
    return 0;
  }
  while (calc != NULL) {
    printf("Rota %d - Destino:  %s\n", count, calc -> nomeRota);
    calc = calc -> prox;
    count++;
  }
  free(calc);

  if (modo == 1) {

    printf("Para escolher digite o numero da rota | Para sair digite 0 \n");
    scanf("%d", & aux);
    getchar();

    if (aux != 0) {
      count = 1;
      ItemListaDeRotas * aux_impressao = * li;
      while (aux_impressao != NULL) {
        if (aux != count) {
          aux_impressao = aux_impressao -> prox;
          count++;
        } else if (aux == count) {
          printf("Iniciando a Rota %s...", aux_impressao -> nomeRota);
          imprime_rota(aux_impressao -> rotaDescritor);
          break;
        }
      }
    }
  }
}

struct nodoRota gera_nodo_lista_de_rotas(Lista * mat, char nRota[50]) {
  struct nodoRota n;
  n.rotaDescritor = mat;
  strcpy(n.nomeRota, nRota);
  //  n.prox = NULL;
  return n;
}
