#include <stdio.h>

#include <string.h>

#include <stdlib.h>

#include "RotaDeOnibus.h"

Lista * cria_lista() {
  Lista * li = (Lista * ) malloc(sizeof(Lista));
  if (li != NULL) {
    li -> inicio = NULL;
    li -> final = NULL;
    li -> qtd = 0;
  }
  return li;
}

void libera_lista(Lista * li) {
  if (li != NULL) {
    Rota * no;
    while ((li -> inicio) != NULL) {
      no = li -> inicio;
      li -> inicio = li -> inicio -> prox;
      free(no);
    }
    free(li);
  }
}

int tamanho_lista(Lista * li) {
  if (li == NULL)
    return 0;
  return li -> qtd;
}

int lista_cheia(Lista * li) {
  return 0;
}

int lista_vazia(Lista * li) {
  if (li == NULL)
    return 1;
  if (li -> inicio == NULL)
    return 1;
  return 0;
}

int insere_lista_inicio(Lista * li, struct nodo al) {
  if (li == NULL)
    return 0;
  Rota * no;
  no = (Rota * ) malloc(sizeof(Rota));
  if (no == NULL)
    return 0;
  strcpy(no -> destino, al.destino);
  strcpy(no -> descricao, al.descricao);
  no -> prox = li -> inicio;
  if (li -> inicio == NULL)
    li -> final = no;
  li -> inicio = no;
  li -> qtd++;
  return 1;
}

int insere_lista_ordenada(Lista * li, struct nodo al) {
  if (li == NULL)
    return 0;
  Rota * no = (Rota * ) malloc(sizeof(Rota));

  strcpy(no -> destino, al.destino);
  strcpy(no -> descricao, al.descricao);
}

int insere_lista_final(Lista * li, struct nodo al) {
  if (li == NULL)
    return 0;
  Rota * no;
  Rota * ctba;

  no = (Rota * ) malloc(sizeof(Rota));
  ctba = (Rota * ) malloc(sizeof(Rota));

  if (no == NULL)
    return 0;
  strcpy(no -> destino, al.destino);
  strcpy(no -> descricao, al.descricao);

  no -> prox = NULL;
  if (li -> inicio == NULL) {
    strcpy(ctba -> destino, "Curitiba");
    strcpy(ctba -> descricao, "Cidade Sorriso, terra do pinh�o");
    printf("");
    no -> ant = ctba;
    ctba -> prox = no;
    li -> inicio = ctba; //lista vazia
    printf("aa");
  } else {
    no -> ant = li -> final; //garante lista duplamente encadeada
    li -> final -> prox = no; //altera elemento do final

  }
  li -> final = no; //altera n� descritor

  li -> qtd++; //altera qtd do n� descritor
  return 1;
}

int remove_lista_inicio(Lista * li) {
  if (li == NULL)
    return 0;
  if (li -> inicio == NULL)
    return 0;

  Rota * no = li -> inicio; //declara n�
  li -> inicio = no -> prox; // aponta n� descritor para o pr�ximo
  free(no);
  if (li -> inicio == NULL)
    li -> final = NULL;
  li -> qtd--;
  return 1;
}

int remove_lista_final(Lista * li) {
  if (li == NULL)
    return 0;
  if (li -> inicio == NULL) //aponta para o n� descritor
    return 0;

  Rota * ant, * no = li -> inicio; // aponta para o n� descritor
  while (no -> prox != NULL) {
    ant = no;
    no = no -> prox;
  }
  //remo��o do in�cio
  if (no == li -> inicio) {
    li -> inicio = NULL;
    li -> final = NULL;
  } else {
    ant -> prox = no -> prox;
    li -> final = ant; //aponta o n� descritor para o anterior
  }
  free(no);
  li -> qtd--; //altera qtd
  return 1;
}

int consulta_lista_pos(Lista * li, int pos, struct nodo * al) {
  if (li == NULL || li -> inicio == NULL || pos <= 0) //verifica descritor
    return 0;
  Rota * no = li -> inicio; //puxa inicio do n�
  int i = 1;
  while (no != NULL && i < pos) {
    no = no -> prox;
    i++;
  }
  if (no == NULL)
    return 0;
  else {
    strcpy(al -> descricao, no -> destino);
    strcpy(al -> descricao, no -> descricao);

    return 1;
  }
}

void imprime_rota(Lista * li) {
  if (li == NULL || li -> inicio == NULL) {
    printf("\n <<Lista Vazia>> \n");
    return;
  }
  int contr, quit, i = 0;
  quit = 1;

  Rota * no = li -> inicio; //aponta para o descritor
  while (quit != 0) {
    system("cls");

    printf("\nCidade %d: %s \n", i + 1, no -> destino);
    printf("Frase:%s\n\n", no -> descricao);
    printf("-------------------------------\n");
    if (no != li -> final) printf("[1] Ir para proxima cidade\n");
    if (no != li -> inicio) printf("[2] Voltar para a cidade anterior\n");
    printf("[0] Voltar ao menu\n");
    scanf("%d", & contr);
    getchar();

    switch (contr) {
    case 1:
      no = no -> prox;
      i++;
      break;
    case 2:
      no = no -> ant;
      i--;
      break;
    case 0:
      quit = 0;
      break;
    }

  }

  printf("\n");
  system("cls");
}
