typedef struct descritor Lista;

struct nodo {
  char destino[50];
  char descricao[200];
  struct nodo * prox;
  struct nodo * ant;
};
typedef struct nodo Rota;

struct descritor {
  struct nodo * inicio;
  struct nodo * final;
  int qtd;
};

Lista * cria_lista();
void libera_lista(Lista * li);
int insere_lista_ordenada(Lista * li, struct nodo al);
int insere_lista_final(Lista * li, struct nodo al);
int insere_lista_inicio(Lista * li, struct nodo al);
int remove_lista_inicio(Lista * li);
int remove_lista_final(Lista * li);
int tamanho_lista(Lista * li);
int lista_vazia(Lista * li);
int lista_cheia(Lista * li);
int consulta_lista_pos(Lista * li, int pos, struct nodo * al);
void imprime_rota(Lista * li);
