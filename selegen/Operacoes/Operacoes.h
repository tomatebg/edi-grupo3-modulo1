#include "../ListaDeMatrizes/ListaDeMatrizes.h"

MatrizEsparsa* operacao_matrizes_mesmo_tamanho(ItemListaDeMatrizes* primeira, ItemListaDeMatrizes* segunda, int* tLinhas, int* tColunas, int tipoOp);

MatrizEsparsa* multiplica_matriz(ItemListaDeMatrizes* primeira, ItemListaDeMatrizes* segunda, int* tLinhas, int* tColunas);

MatrizEsparsa* matriz_transposta(ItemListaDeMatrizes* matriz, int* tLinhas, int* tColunas);