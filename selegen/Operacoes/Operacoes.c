#include <stdio.h>

#include <stdlib.h>

#include "Operacoes.h"

MatrizEsparsa* operacao_matrizes_mesmo_tamanho(ItemListaDeMatrizes* primeira, ItemListaDeMatrizes* segunda, int* tLinhas, int* tColunas, int tipoOp) {
    //tipoOp = 0 -> subtração
    //tipoOp = 1 -> adição

    if (primeira == NULL || segunda == NULL) {
        printf("Erro: Matrizes inv�lidas!");
        return NULL;
    }
    if(primeira->totalColunas != segunda->totalColunas || primeira->totalLinhas != segunda->totalLinhas) {
        printf("Erro: Matrizes n�o tem a mesma dimens�o!");
        return NULL;
    }
    if(tipoOp != 0 && tipoOp != 1){
        printf("Erro: Parâmetro de opera��o inv�lido!");
        return NULL;
    }

    int linhas = primeira->totalLinhas;
    int colunas = primeira->totalColunas;

    MatrizEsparsa* matriz = cria_matriz();
    if (matriz == NULL)
        return NULL;
    *tLinhas = linhas;
    *tColunas = colunas;
    float primeiroDado, segundoDado;
    float resultado;
    
    for(int l = 1; l <= linhas; ++l) {
        for(int c = 1; c <= colunas; ++c) {
            consulta_matriz_pos(primeira->matriz, l, c, &primeiroDado);
            consulta_matriz_pos(segunda->matriz, l, c, &segundoDado);

            if(primeiroDado != 0 || segundoDado != 0){
                if(tipoOp == 0) {
                    resultado = primeiroDado - segundoDado;
                }
                else{
                    resultado = primeiroDado + segundoDado;
                }
                if(resultado != 0) {
                    insere_matriz_ordenada(matriz, gera_nodo_matriz((primeiroDado + segundoDado), l, c), l, c); 
                }
            }
        }
    }
    return matriz;
}

MatrizEsparsa* multiplica_matriz(ItemListaDeMatrizes* primeira, ItemListaDeMatrizes* segunda, int* tLinhas, int* tColunas) {
    if (primeira == NULL || segunda == NULL) {
        printf("Erro: Matrizes inv�lidas!");
        return NULL;
    }
    if(primeira->totalColunas != segunda->totalLinhas) {
        printf("Erro: Numero de colunas da primeira matriz � diferente do n�mero de linhas da segunda!");
        return NULL;
    }

    MatrizEsparsa* matriz = cria_matriz();
    if (matriz == NULL)
        return NULL;
    *tLinhas = primeira->totalLinhas;
    *tColunas = segunda->totalColunas;
    float primeiroDado, segundoDado;
    float dado;

    // resultante = M p->tL x s->tC
    for (int l = 1; l <= primeira->totalLinhas; ++l) {
        for(int c = 1; c <= segunda->totalColunas; c ++) {
            dado = 0;
            for (int ll = 1; ll <= segunda->totalLinhas; ll++) {    
                consulta_matriz_pos(primeira->matriz, l, ll, &primeiroDado);
                consulta_matriz_pos(segunda->matriz, ll, c, &segundoDado);
                dado += primeiroDado * segundoDado;
            }
            if(dado != 0) {
                insere_matriz_ordenada(matriz, gera_nodo_matriz(dado, l, c), l, c); //insere_matriz_inicio provisorio
            }
        }
    }
    return matriz;
}

MatrizEsparsa* matriz_transposta(ItemListaDeMatrizes* matriz, int* tLinhas, int* tColunas) {
    if (matriz == NULL) {
        printf("Erro: Matriz inv�lida!");
        return NULL;
    }

    MatrizEsparsa* matrizResultado = cria_matriz();
    if (matriz == NULL)
        return NULL;
    *tLinhas = matriz->totalColunas;
    *tColunas = matriz->totalLinhas;
    float dado;

    for (int l = 1; l <= matriz->totalLinhas; ++l) {
        for(int c = 1; c <= matriz->totalColunas; c ++) {
            dado = 0;
            consulta_matriz_pos(matriz->matriz, l, c, &dado);
            if(dado != 0) {
                insere_matriz_ordenada(matrizResultado, gera_nodo_matriz(dado, c, l), c, l); //insere_matriz_inicio provisorio
            }
        }
    }
    return matrizResultado;
}
