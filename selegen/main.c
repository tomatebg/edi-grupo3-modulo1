#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "Operacoes/Operacoes.h"

void loopDeOperacoes(ListaDeMatrizes* listaDeMatrizes);

int main(){
	setlocale(LC_ALL, "");
	struct nodo a;
	struct nodoMatriz b;
    ListaDeMatrizes* listaDeMatrizes;
		
	int i, contr, opt1, opt2, insert;
    opt1 = -1;
	float dado;

	printf("Bem vindo\n");

	while (opt1 != 0) {
		i = contr = opt1 = 0;
		opt2 = -1;

		printf("[1] Cria��o e Libera��o\n");
		printf("[2] Inser��o\n");
		printf("[3] Remo��o de lista\n");
		printf("[4] Consulta de itens\n");
		printf("[5] Visualiza��es de matriz\n");
		printf("[6] Opera��es\n");
		printf("[0] Sair \n");
		scanf("%d", &opt1);
		
		switch (opt1)
		{
			case 0:
				return 0;
			case 1:
				system("cls");

				printf("[11] Criar a lista de matrizes \n");
				printf("[12] Criar uma nova matriz \n");
				printf("[13] Liberar lista de matrizes\n");
				scanf("%d", &contr);
				opt2 = contr;
				system("cls");
				break;
			case 2:
				opt2 = 22;
				system("cls");
				break;

			case 3:
				system("cls");
				opt2 = 31;
				break;
			case 4:
				system("cls");
				opt2 = 44;
				break;
			case 5:
				system("cls");
				printf("[51] Imprimir Matriz Esparsa \n");
				printf("[52] Imprimir diagonal principal\n");
				scanf("%d", &contr);

				opt2 = contr;
				break;
			case 6:
				system("cls");
				if(tamanho_lista_de_matrizes(listaDeMatrizes) > 0) {
					loopDeOperacoes(listaDeMatrizes);
				}
				break;
		}
		
		switch (opt2)
		{
			case 0:
				opt1 = 0;
				break;
			case 11:
				system("cls");

				listaDeMatrizes = cria_lista_de_matrizes();
				printf("Lista Iniciada \n");
				break;
			case 12:
				system("cls");

				i=1;
				while (i != 0) {
					printf("Digite a quantidade de linhas (m) da matriz: ");
					scanf("%d", &b.totalLinhas);
					printf("Digite a quantidade de colunas (n) da matriz: ");
					scanf("%d", &b.totalColunas);
					if (b.totalLinhas < 1 || b.totalColunas < 1){
						printf("Linha/coluna invalida\n");
					}
					else {
						i = 0;
					}
				}
				
				insert = 2;
				while(insert != 0 && insert != 1) {
					printf("Deseja inserir os itens da matriz?: \n");
					printf("[0] N�o\n");
					printf("[1] Sim\n");
					scanf("%d", &insert);
					if(insert != 0 && insert != 1) {
						printf("Leitura inv�lida\n");
					}
				}

				MatrizEsparsa* matriz;
				matriz = cria_matriz();

				while(insert != 0) {
					printf("Digite o numero que deseja inserir (0 para finalizar): ");
					scanf("%f", &a.dado);
					if(a.dado == 0) {
						insert = 0;
						break;
					}
					printf("LINHA: ");
					scanf("%d", &a.lin);
					printf("COLUNA: ");
					scanf("%d", &a.col);
					if (a.lin < 0 || a.col < 0){
						printf("Linha/coluna invalida\n");
					}
					else{
						contr = insere_matriz_ordenada(matriz, gera_nodo_matriz(a.dado, a.lin, a.col), b.totalLinhas, b.totalColunas); 
						system("cls");

						if (contr == 0) printf("Matriz Lotada ou posi��o j� ocupada\n");
						if (contr != 0 && contr != -1) printf("Inserido com sucesso\n");
						if (contr == -1) printf("Imposs�vel inserir. Fora do limite da matriz!\n");		
					}
				}
				insere_lista_de_matrizes(listaDeMatrizes, gera_nodo_lista_de_matrizes(matriz, b.totalLinhas, b.totalColunas));
				
				break;
			case 13:
				system("cls");

				libera_lista_de_matrizes(listaDeMatrizes);
				listaDeMatrizes = cria_lista_de_matrizes();
				printf("Lista Liberada \n");
				break;
			case 22:
				system("cls");
				
				printf("ESCOLHA A MATRIZ NA QUAL DEVE SER FEITA A INSER��O:");
				imprime_lista_de_matrizes(listaDeMatrizes, 0);
		
				i = -1;
				if (tamanho_lista_de_matrizes(listaDeMatrizes)==0 ) break;  
				while (i < 0 && i < tamanho_lista_de_matrizes(listaDeMatrizes))
				{

					scanf("%d", &i);
					if( i > tamanho_lista_de_matrizes(listaDeMatrizes)) {
						printf("A Matriz inserida � inv�lida! \n");
							i = -1;
					}
				}
				
				MatrizEsparsa* matrizMod;
				matrizMod = retorna_matriz_em_indice(listaDeMatrizes, i)->matriz;
				i=1;
				while (i != 0){
					printf("Digite o numero que deseja inserir: \n");
					scanf("%f", &a.dado);
					printf("LINHA: \n");
					scanf("%d", &a.lin);
					printf("COLUNA: \n");
					scanf("%d", &a.col);
					if (a.lin < 0 || a.col < 0){
						printf("Linha/coluna invalida\n");
					}
					else {
						i = 0;
					}
				}

				contr = insere_matriz_ordenada(matrizMod, a ,b.totalLinhas, b.totalColunas);

				if (contr == 0) printf("Lista Lotada\n");
				if (contr != 0) printf("Inserido com sucesso\n");
				break;
		
			case 31:
				system("cls");
				printf("- REMO��O DE LISTA DE MATRIZES -  \n");

				imprime_lista_de_matrizes(listaDeMatrizes, 0);

				printf("Qual item deseja remover? \n");
				scanf("%d", &i);

				contr = remove_indice_lista_de_matrizes(listaDeMatrizes, i); ///mudar
				system("cls");

				printf("Removido com sucesso\n");
				break;
			
		
			case 44:
				system("cls");
				printf("ESCOLHA A MATRIZ NA QUAL DESEJA PROCURAR:");
				insert = imprime_lista_de_matrizes(listaDeMatrizes, 0);
				
				i = -1;
				while (i < 0 || i > tamanho_lista_de_matrizes(listaDeMatrizes) + 1)
				{
					printf("Digite a matriz desejada:\n");
					scanf("%d", &i);
					if(i < 0 || i > tamanho_lista_de_matrizes(listaDeMatrizes) + 1) {
						printf("Posi��o inv�lida!");
					}
				}
				
				MatrizEsparsa* matrizConsulta;
				matrizConsulta = retorna_matriz_em_indice(listaDeMatrizes, i)->matriz;

				dado = 0;
				int linha, coluna;
				linha = coluna = 0;
				printf("Digite a linha que deseja procurar:\n");
				scanf("%d", &linha);
				printf("Digite a coluna que deseja procurar:\n");
				scanf("%d", &coluna);
				contr = consulta_matriz_pos(matrizConsulta, linha, coluna, &dado);
				//tratamento do contr
				printf("O valor na posi��o escolhida �:  \n");
				printf(" %f\n", dado);
				break;
				
			case 51:
				system("cls");

				printf("- IMPRIMIR LISTA -  \n");
				imprime_lista_de_matrizes(listaDeMatrizes, 1);

				break;
				
			case 52:
				system("cls");

				printf("- IMPRIMIR DIAGONAL PRINCIPAL -  \n");
				imprime_lista_de_matrizes(listaDeMatrizes, 2);

				break;
		}
	}
    return 0;
}

void loopDeOperacoes(ListaDeMatrizes* listaDeMatrizes) {
	ItemListaDeMatrizes* primeiraMatriz;
	ItemListaDeMatrizes* segundaMatriz;
	ItemListaDeMatrizes* matrizResultado;
	MatrizEsparsa* resultado;
	int linhasResultado, colunasResultado, i, opt;
	int continua = 1;

	while (continua > 0) {
		primeiraMatriz = NULL;
		segundaMatriz = NULL;
		resultado = NULL;
		printf("[61] Somar duas matrizes \n");
		printf("[62] Subtrair duas matrizes\n");
		printf("[63] Multiplicar duas matrizes\n");
		printf("[64] Gerar matriz transposta\n");
		scanf("%d", &opt);

		switch (opt) {
			case 61:
				printf("Soma de matrizes.\n");
				break;
			case 62:
				printf("Subtra��o de matrizes.\n");
				break;
			case 63:
				printf("Multiplica��o de matrizes.\n");
				break;
			case 64:
				printf("Transposta de matriz.\n");
				break;
		}

		imprime_lista_de_matrizes(listaDeMatrizes, 0);
		if(continua == 1 && (opt == 61 || opt == 62 || opt == 63)) {
			int prim, seg;
			prim = -1;
			while (prim < 0 || prim > tamanho_lista_de_matrizes(listaDeMatrizes) + 1)
			{
				printf("Escolha a primeira matriz para realizar a opera��o:");
				scanf("%d", &prim);
				if(prim < 0 && prim > tamanho_lista_de_matrizes(listaDeMatrizes)) {
					printf("Posi��o inv�lida!");
				}
			}

			seg = -1;
			while (seg < 0 || seg > tamanho_lista_de_matrizes(listaDeMatrizes) + 1)
			{
				printf("Escolha a segunda matriz para realizar a opera��o:");
				scanf("%d", &seg);
				if(seg < 0 || seg > tamanho_lista_de_matrizes(listaDeMatrizes) + 1) {
					printf("Posi��o inv�lida!");
				}
			}
			
			primeiraMatriz = retorna_matriz_em_indice(listaDeMatrizes, prim);
			segundaMatriz = retorna_matriz_em_indice(listaDeMatrizes, seg);
		}
		if((continua == 2 && (opt == 61 || opt == 62 || opt == 63)) || opt == 64) {
			int ref;
			ref = -1;
			while (ref < 0 || ref > tamanho_lista_de_matrizes(listaDeMatrizes) + 1)
			{
				printf("Escolha a matriz para realizar a opera��o:");
				scanf("%d", &ref);
				if(ref < 0 || ref > tamanho_lista_de_matrizes(listaDeMatrizes) + 1) {
					printf("Posi��o inv�lida!");
				}
			}
			primeiraMatriz = retorna_matriz_em_indice(listaDeMatrizes, ref);
		}

		switch (opt) {
			case 61:
				if(continua == 1) {
					//1 -> soma	
					resultado = operacao_matrizes_mesmo_tamanho(primeiraMatriz, segundaMatriz, &linhasResultado, &colunasResultado, 1);
				}
				else {
					resultado = operacao_matrizes_mesmo_tamanho(matrizResultado, primeiraMatriz, &linhasResultado, &colunasResultado, 1);
				}
				break;
			case 62:
				if(continua == 1) {
					//0 -> subtração	
					resultado = operacao_matrizes_mesmo_tamanho(primeiraMatriz, segundaMatriz, &linhasResultado, &colunasResultado, 0);
				}
				else {
					resultado = operacao_matrizes_mesmo_tamanho(matrizResultado, primeiraMatriz, &linhasResultado, &colunasResultado, 0);
				}
				break;
			case 63:
				if(continua == 1) {
					resultado = multiplica_matriz(primeiraMatriz, segundaMatriz, &linhasResultado, &colunasResultado);
				}
				else {
					resultado = multiplica_matriz(matrizResultado, primeiraMatriz, &linhasResultado, &colunasResultado);
				}
				break;
			case 64:
				if(continua == 1) {
					resultado = matriz_transposta(primeiraMatriz, &linhasResultado, &colunasResultado);
				}
				else {
					resultado = matriz_transposta(matrizResultado, &linhasResultado, &colunasResultado);
				}
				break;
			default:
				printf("Op��o inv�lida!\n");
				break;
		}

		if(resultado != NULL) {
			i = -1;
			while (i < 0 || i > 2)
			{
				printf("Opera��o realizada com sucesso! O que deseja realizar com a matriz resultante?\n");
				printf("[0] Descartar\n");
				printf("[1] Armazenar\n");
				printf("[2] Continuar realizando opera��es\n");
				scanf("%d", &i);
				if(i < 0 || i > 2) {
					printf("Escolha inv�lida!");
				}
			}

			switch (i){
				case 0:
					continua = 0;
				case 1:
					insere_lista_de_matrizes(listaDeMatrizes, gera_nodo_lista_de_matrizes(resultado, linhasResultado, colunasResultado));
					continua = 0;
					break;
				case 2:
					matrizResultado = aloca_nodo_lista_de_matrizes();
					matrizResultado->matriz = resultado;
					matrizResultado->totalLinhas = linhasResultado;
					matrizResultado->totalColunas = colunasResultado;
					continua = 2;
					break;
			}
		}
		else{
			printf("A opera��o falhou!");
			return;
		}
	}
	return;
}
