//Arquivo MatrizEsparsa.h
typedef struct nodo{
    float dado;
    int lin, col;
    struct nodo *prox;
} ElementoDeMatriz;

typedef struct nodo* MatrizEsparsa;

MatrizEsparsa* cria_matriz();
void libera_matriz(MatrizEsparsa* li);
ElementoDeMatriz* aloca_nodo_matriz();
int insere_matriz_ordenada(MatrizEsparsa* li, struct nodo al, int lin, int col);
int remove_matriz_conteudo(MatrizEsparsa* li, int mat);
int tamanho_matriz(MatrizEsparsa* li);
void imprime_matriz(MatrizEsparsa * li, int lin, int col);
int consulta_matriz_mat(MatrizEsparsa* li, int mat, struct nodo *al);
int consulta_matriz_pos(MatrizEsparsa* li, int linha, int coluna, float *dado);
struct nodo gera_nodo_matriz(float dado, int lin, int col);
void imprime_matriz_diagonal(MatrizEsparsa* li, int lin, int col);
MatrizEsparsa* soma_de_matrizes(MatrizEsparsa* primeira, MatrizEsparsa* segunda);
