//Matriz esparsa.c
#include <stdio.h>

#include <stdlib.h>

#include "MatrizEsparsa.h" //inclui os Prot?tipos

MatrizEsparsa * cria_matriz() {
  MatrizEsparsa * li = (MatrizEsparsa * ) malloc(sizeof(MatrizEsparsa));
  if (li != NULL)
    *
    li = NULL;
  return li;
}

void libera_matriz(MatrizEsparsa * li) {
  if (li != NULL) {
    ElementoDeMatriz * no;
    while (( * li) != NULL) {
      no = * li;
      * li = ( * li) -> prox;
      free(no);
    }
    free(li);
  }
}

int consulta_matriz_pos(MatrizEsparsa * li, int linha, int coluna, float * dado) {
  if (li == NULL || linha <= 0 || coluna <= 0)
    return 0;
  ElementoDeMatriz * no = * li;

  * dado = 0;
  while (no != NULL) {
    if (no -> lin == linha && no -> col == coluna) {
      * dado = no -> dado;
      return 1;
    }
    no = no -> prox;
  }
  return 0;
}

int consulta_matriz_mat(MatrizEsparsa * li, int mat, struct nodo * al) {
  if (li == NULL)
    return 0;
  ElementoDeMatriz * no = * li;

  while (no != NULL && no -> dado != mat) {
    no = no -> prox;
  }
  if (no == NULL)
    return 0;
  else {
    //        *al = no->dado;
    return 1;
  }
}

ElementoDeMatriz * aloca_nodo_matriz() {
  ElementoDeMatriz * el = (ElementoDeMatriz * ) malloc(sizeof(ElementoDeMatriz));
  return el;
}

int insere_matriz_ordenada(MatrizEsparsa * li, struct nodo al, int lin, int col) {
  int linha_atual, coluna_atual;
  if (li == NULL)
    return 0;
  ElementoDeMatriz * no = (ElementoDeMatriz * ) malloc(sizeof(ElementoDeMatriz));
  if (no == NULL)
    return 0;

  if (al.lin > lin && al.col > col) {
    return -1;
  }
  no -> dado = al.dado;
  no -> lin = al.lin;
  no -> col = al.col;

  if (( * li) == NULL) { //matriz vazia: insere in�cio
    no -> prox = NULL;
    * li = no;

    return 1;
  } else {

    ElementoDeMatriz * ant, * atual = * li;
    ant = * li;

    while (atual != NULL && atual -> lin <= al.lin && atual -> col <= al.col) { //BUG: 8 (4:1)
      if (atual -> lin == al.lin && atual -> col == al.col) {
        printf("J� existe um valor na posi��o escolhida"); // Bloqueio de inser��o dupla 
        return 0;
      }

      ant = atual;
      atual = atual -> prox;
    }

    if (atual == * li) { //insere in�cio
 
  if( atual -> lin < al.lin){
      while (atual != NULL && atual -> lin < al.lin) { //trata coluna menor que
        ant = atual;
        atual = atual -> prox;
      }
      no -> prox = atual;
      ant -> prox = no;
    }else {
    	no -> prox = ( * li);
        * li = no;
		}
 
    return 1;

    } else { // abordagem padr�o
    printf("c");
      no -> prox = atual;
      ant -> prox = no;

    }
    return 1;
  }
}


int remove_matriz_conteudo(MatrizEsparsa * li, int mat) {
  if (li == NULL) // Verifica se matriz existe
    return 0;
  if (( * li) == NULL) //Verifica se a matriz � vazia
    return 0;
  ElementoDeMatriz * ant, * no = * li;
  while (no != NULL && no -> dado != mat) {
    ant = no;
    no = no -> prox;
  }
  if (no == NULL) //Chega ao final
    return 0;

  if (no == * li) //apenas um nodo
    *
    li = no -> prox;
  else
    ant -> prox = no -> prox;
  free(no);
  return 1;
}

int tamanho_matriz(MatrizEsparsa * li) {
  if (li == NULL)
    return 0;
  int cont = 0;
  ElementoDeMatriz * no = * li;
  while (no != NULL) {
    cont++;
    no = no -> prox;
  }
  return cont;
}

void imprime_matriz_diagonal(MatrizEsparsa * li, int lin, int col) {
  if (li == NULL)
    return;
  int i;
  float aux;

  if (lin <= col) {
    i = col;
  } else {
    i = lin;
  }

  ElementoDeMatriz * no = * li;
  printf("A diagonal principal � composta por: \n");

  for (int a = 1; i >= a; a++) {
    aux = 0;
    while (no != NULL) {
      if (a == no -> lin && a == no -> col) {
        aux = no -> dado;
      }
      no = no -> prox;
    }
    no = * li;
    aux == 0 ? printf("0\t") : printf("%.2f\t ", aux);
  }
}

void imprime_matriz(MatrizEsparsa * li, int lin, int col) {
  int zero, tam, c, l, linhas_executadas, colunas_vazias;
  tam = l = zero = 0;
  linhas_executadas = 1;
  ElementoDeMatriz * no = * li;
  ElementoDeMatriz * calc = * li;

  if (li == NULL)
    return;
  int i = 0;

  tam = tamanho_matriz(li);
  printf("A MatrizEsparsa possui %d elementos \n", tam);

  while (calc != NULL) {

    calc = calc -> prox;
  }
  free(calc);

  while (no != NULL) {
    if (calc != NULL && no -> lin == calc -> lin) { // Verifica se o elemento atual est� no mesmo da linha anterior
      c = calc -> col + 1;
    } else {
      c = 1;
    }

    if (no -> lin == linhas_executadas) {
      while (c < (no -> col)) { // carrega zeros na esquerda e o n�mero
        printf(" %-6d ", zero);
        c++;
      }

      printf(" %-6.2f ", no -> dado);

      if (no -> col == col) {
        linhas_executadas++; //corrige bug de linha extra
        printf("\n");
      }

    } else {

      while (no -> lin > linhas_executadas) { // imprime linhas vazias e o numero ap�s elas

        while (c <= col) {
          printf(" %-6d ", zero);
          c++;
        }
        linhas_executadas++;
        c = 1;

        printf("\n");

      }

      while (c < no -> col) { // carregas zeros na esquerda e o n�mero
        printf(" %-6d ", zero);
        c++;
      }
      printf(" %-6.2f ", no -> dado);
      if (no-> prox == NULL && c== col)  printf("\n");
    }

    calc = no;
    no = no -> prox;

    if (no != NULL && no -> lin != calc -> lin) { // caso o prox elemento fique na prox linha

      if (c < col) {
        while (c < col) {
          printf(" %-6d ", zero);
          //corrige zeros at� completar a linha
          c++;
        }
        printf("\n");
        linhas_executadas++;
      }
    }
    i++;
  }

  //PREENCHE LINHAS VAZIAS NO FIM
  if (linhas_executadas < lin || c < col) {
    if (c < col) {
      while (c < col) {
        printf(" %-6d ", zero);
        c++;
      }
      printf("\n");
    }
    while (linhas_executadas < lin) {
      int aux = 1;
      while (aux <= col) {
        printf(" %-6d ", zero);
        aux++;
      }
      printf("\n");

      linhas_executadas++;
    }
  }

  if (i == 0) {
    printf("\n <<MatrizEsparsa Vazia>> \n");
  } else {

  };
  printf("\n");
}

struct nodo gera_nodo_matriz(float dado, int lin, int col) {
  struct nodo n;
  n.dado = dado;
  n.lin = lin;
  n.col = col;
  n.prox = NULL;
  return n;
}
