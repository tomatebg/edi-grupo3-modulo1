#include <stdio.h>

#include <stdlib.h>

#include "ListaDeMatrizes.h" //inclui os Prot?tipos

ListaDeMatrizes * cria_lista_de_matrizes() {
  ListaDeMatrizes * li = (ListaDeMatrizes * ) malloc(sizeof(ListaDeMatrizes));
  if (li != NULL)
    *li = NULL;
  return li;
}

void libera_lista_de_matrizes(ListaDeMatrizes * li) {
  if (li != NULL) {
    ItemListaDeMatrizes * no;
    while (( * li) != NULL) {
      no = * li;
      * li = ( * li) -> prox;
      free(no);
    }
    free(li);
  }
}

int consulta_lista_de_matrizes_pos(ListaDeMatrizes * li, int pos, struct nodoMatriz * al) {
  if (li == NULL || pos <= 0)
    return 0;
  ItemListaDeMatrizes * no = * li;
  int i = 1;
  while (no != NULL && i < pos) {
    no = no -> prox;
    i++;
  }
  if (no == NULL)
    return 0;
  else {
    //        *al = no->dado;
    return 1;
  }
}

int consulta_lista_de_matrizes_mat(ListaDeMatrizes * li, MatrizEsparsa* mat, struct nodoMatriz * al) {
  if (li == NULL)
    return 0;
  ItemListaDeMatrizes* no = * li;

  while (no != NULL && no -> matriz != mat) {
    no = no -> prox;
  }
  if (no == NULL)
    return 0;
  else {
    //        *al = no->dado;
    return 1;
  }
}

ItemListaDeMatrizes* aloca_nodo_lista_de_matrizes() {
  ItemListaDeMatrizes * el = (ItemListaDeMatrizes * ) malloc(sizeof(ItemListaDeMatrizes));
  return el;
}

int insere_lista_de_matrizes(ListaDeMatrizes * li, struct nodoMatriz al) {
  //sempre no final, por ordem de cria??o
  if (li == NULL)
    return 0;
  ItemListaDeMatrizes* no = aloca_nodo_lista_de_matrizes();
  if (no == NULL)
    return 0;
  no -> matriz = al.matriz;
  no -> totalLinhas = al.totalLinhas;
  no -> totalColunas = al.totalColunas;

  no -> prox = NULL;
  if (( * li) == NULL) { //lista vazia: insere in�cio
    * li = no;
  } else {
    ItemListaDeMatrizes * aux;
    aux = * li;
    while (aux -> prox != NULL) {
      aux = aux -> prox;
    }
    aux -> prox = no;
  }
  return 1;
}

int remove_lista_de_matrizes(ListaDeMatrizes* li, MatrizEsparsa* mat) {
  if (li == NULL) // Verifica se lista existe
    return 0;
  if (( * li) == NULL) //Verifica se a lista � vazia
    return 0;
  ItemListaDeMatrizes * ant, * no = * li;
  while (no != NULL && no -> matriz != mat) {
    ant = no;
    no = no -> prox;
  }
  if (no == NULL) //Chega ao final
    return 0;

  if (no == * li) //apenas um nodoMatriz
    *
    li = no -> prox;
  else
    ant -> prox = no -> prox;
  free(no);
  return 1;
}

int remove_indice_lista_de_matrizes(ListaDeMatrizes * li, int indice) {
  if (li == NULL) // Verifica se lista existe
    return 0;
  if (( * li) == NULL) //Verifica se a lista � vazia
    return 0;
  ItemListaDeMatrizes * ant, * no = * li;
  int cont = 1;
  while (no != NULL && indice != cont) {
    ant = no;
    no = no -> prox;
    cont++;
  }
  if (no == NULL) //Chega ao final
    return 0;

  if (no == * li) {
    *li = no -> prox; //apenas um nodoMatriz
  }
  else{
    ant -> prox = no -> prox;
  }
  free(no);
  return 1;
}

int tamanho_lista_de_matrizes(ListaDeMatrizes * li) {
  if (li == NULL)
    return 0;
  int cont = 0;
  ItemListaDeMatrizes * no = * li;
  while (no != NULL) {
    cont++;
    no = no -> prox;
  }
  return cont;
}

ItemListaDeMatrizes* retorna_matriz_em_indice(ListaDeMatrizes* li, int indice) {
  if (li == NULL)
    return 0;
  int cont = 1;
  ItemListaDeMatrizes* no = *li;
  while (no != NULL) {
    if(cont == indice) {
      return no;
    }
    cont++;
    no = no -> prox;
  }
  return NULL;
}

int lista_de_matrizes_cheia(ListaDeMatrizes * li) {
  return 0;
}

int lista_de_matrizes_vazia(ListaDeMatrizes * li) {

  if (li == NULL)
    return 1;
  if ( * li == NULL)
    return 1;
  return 0;
}

int imprime_lista_de_matrizes(ListaDeMatrizes * li, int modo) {
  int aux;
  if (li == NULL)
    return 0;
  int i = 0;

  int tam = tamanho_lista_de_matrizes(li);
  printf("A Lista de Matrizes possui %d elementos \n", tam);

  ItemListaDeMatrizes * calc = * li;
  int count = 1;
  if (calc == NULL) {
    printf("\nLista de Matrizes vazia\n");
    return 0;
  }
  while (calc != NULL) {
    printf("Matriz %d: %dx%d\n", count, calc->totalLinhas, calc->totalColunas);
    calc = calc -> prox;
    count++;
  }
  free(calc);
  
  //MODO 0 impress�o por outras fun��es
  //MODO 1 impress�o solicitada pelo usuario
  //MODO 2 impressao diagonal
  
  if (modo== 1 ){
    //Impress�o da matriz esparsa
  
		printf("Para imprimir digite o numero da matriz | Para sair digite 0 \n");
	 	scanf("%d", &aux);
	 	
	 	if(aux!=0){
	 		count = 1;
	 		ItemListaDeMatrizes * aux_impressao = * li;
	   		while (aux_impressao != NULL) {
	   			if (aux != count){
	   				aux_impressao = aux_impressao -> prox;
	    			count++;
				}else if (aux == count){
					printf("Imprimindo a Matriz Esparsa %d...\n", aux);
					imprime_matriz(aux_impressao->matriz, aux_impressao->totalLinhas, aux_impressao->totalColunas );
					break;
				}
	 	 	}
		}
    else return 0;
	 }
   else if (modo == 2){
    //Impress�o dos valores diagonais
    	printf("Para imprimir digite o numero da matriz desejada | Para sair digite 0 \n");
    	scanf("%d", &aux);
	 		count = 1;
	 		ItemListaDeMatrizes* aux_impressao = * li;
      if (aux_impressao == NULL) return 0;

      while (aux_impressao != NULL) {
        if (aux != count){
          aux_impressao = aux_impressao -> prox;
          count++;
				} else if (aux == count){
					imprime_matriz_diagonal(aux_impressao->matriz, aux_impressao->totalLinhas, aux_impressao->totalColunas);
				}
	 	 	}
	 }
  return 1;
  printf("\n");
}

struct nodoMatriz gera_nodo_lista_de_matrizes(MatrizEsparsa* mat, int tLinhas, int tColunas) {
  struct nodoMatriz n;
  n.matriz = mat;
  n.totalLinhas = tLinhas;
  n.totalColunas = tColunas;
  n.prox = NULL;
  return n;
}
