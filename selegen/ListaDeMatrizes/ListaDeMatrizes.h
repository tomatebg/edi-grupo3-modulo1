//Lista de Matrizes
#include "../MatrizEsparsa/MatrizEsparsa.h"

typedef struct nodoMatriz {
    MatrizEsparsa* matriz;
    int totalLinhas, totalColunas;
    struct nodoMatriz *prox;
} ItemListaDeMatrizes;

typedef struct nodoMatriz* ListaDeMatrizes;

ListaDeMatrizes* cria_lista_de_matrizes();
void libera_lista_de_matrizes(ListaDeMatrizes* li);
ItemListaDeMatrizes* aloca_nodo_lista_de_matrizes();
int insere_lista_de_matrizes(ListaDeMatrizes* li, struct nodoMatriz al);
int remove_lista_de_matrizes(ListaDeMatrizes* li, MatrizEsparsa* mat);
int remove_indice_lista_de_matrizes(ListaDeMatrizes * li, int indice);
int tamanho_lista_de_matrizes(ListaDeMatrizes* li);
ItemListaDeMatrizes* retorna_matriz_em_indice(ListaDeMatrizes* li, int indice);
int lista_de_matrizes_vazia(ListaDeMatrizes* li);
int lista_de_matrizes_cheia(ListaDeMatrizes* li);
int imprime_lista_de_matrizes(ListaDeMatrizes* li, int modo);
int consulta_lista_de_matrizes_mat(ListaDeMatrizes* li, MatrizEsparsa* mat, struct nodoMatriz *al);
int consulta_lista_de_matrizes_pos(ListaDeMatrizes* li, int pos, struct nodoMatriz *al);
struct nodoMatriz gera_nodo_lista_de_matrizes(MatrizEsparsa* mat, int tLinhas, int tColunas);
